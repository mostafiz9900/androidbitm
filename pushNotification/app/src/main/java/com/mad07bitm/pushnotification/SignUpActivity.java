package com.mad07bitm.pushnotification;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mad07bitm.pushnotification.databinding.ActivitySignUpBinding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SignUpActivity extends AppCompatActivity {
    private List<User> userList;
    private ActivitySignUpBinding signUpBinding;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        signUpBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        init();
       signUpBinding.signupBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               userRegistation();
           }
       });
    }

    private void userRegistation() {
        final String name = signUpBinding.nameEt.getText().toString().trim();
        final String email = signUpBinding.emailEt.getText().toString().trim();
        final String password = signUpBinding.passwordEt.getText().toString().trim();

        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    String userId = firebaseAuth.getCurrentUser().getUid();
                    DatabaseReference reference = databaseReference.child("userSignup").child(userId);
                    User user=new User(name,email);
                    reference.setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                            startActivity(intent);
                        }
                    });


                }else {
                    Toast.makeText(SignUpActivity.this, "unsuccessful", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void init() {
        userList=new ArrayList<>();
        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();

    }
}
